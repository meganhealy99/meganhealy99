/*
 * TimerA_Lib.c
 * init for timerA instances
 */

#include "msp.h"
#include "Lib.h"

void emitter_init() //Init IR emitter on pin 5.6
{
        P5->SEL0 |= BIT6;
        P5->SEL1 &= ~BIT6;
        P5->DIR |= BIT6; //P2.5 for TA0.2
        P5->REN |= BIT6; //Resistor Enable
        P5->OUT |= BIT6; //Internal pull up resistor
}


void receiver_init() //P5.6 for timer capture modes
{
    P2->SEL0 |= (BIT5);
    P2->SEL1 &= ~(BIT5);
    P2->DIR &= ~(BIT5); //P2.4 and 2.5 for TA0.1 and TA0.2
    P2->REN |= BIT5;
    P2->OUT |= BIT5;

    //interrupt:
    P2->IES |= (BIT5); //Set pin interrupt to trigger when it goes from high to low
    P2->IE |= (BIT5); //Enable interrupt for P2.4 & P2.5
    P2->IFG = 0;    //Clear all P2 interrupt flags
}


void Photo_Diode_init() //Set up P6.5 as input to test for IR detection (Part 2 bullet point 3 only)
 {
 P2->SEL0 &= ~BIT5; //Configure P2.5 as I/O
 P2->SEL1 &= ~BIT5;
 P2->DIR &= ~BIT5; //Set P2.5 as input
 P2->REN |= BIT5; //Resistor Enable
 P2->OUT |= BIT5; //Internal pull up resistor
 }


void LED_init() //Init on board red LED
{

    P2->SEL1 &= ~(BIT0 ); //LED P2.0 as output
    P2->SEL0 &= ~(BIT0 ); //LED P2.0 as output
    P2->DIR |= (BIT0 ); //LED P2.0 as output
    P2->OUT &= ~(BIT0 ); //Initialize LEDs off
}

//EMIT 10 Hz frequency:
void timerA_10Hz() //use P2.5
{

    TIMER_A2->CCR[0] = 38500; //10 Hz period
    TIMER_A2->CCTL[1] = (0x00E0); //Configure TIMER_A-0 Channel-2 to reset/set mode
    TIMER_A2->CTL = (0x02D4); //Clock source = SMCLK, clock division = /8, counting mode = up

    TIMER_A2->CCR[1] = (38500 / 2); //sets 50% duty cycle for TA0.2


}

void timerA2_init()
{
    TIMER_A2->CTL |=
            TIMER_A_CTL_TASSEL_2 |
            TIMER_A_CTL_MC__UP |
            TIMER_A_CTL_ID_3 |
            TIMER_A_CTL_CLR;

    TIMER_A2->CCTL[1] =
            TIMER_A_CCTLN_CM_1 |
            TIMER_A_CCTLN_CCIS_0 |
            TIMER_A_CCTLN_CCIE |
            TIMER_A_CCTLN_CAP |
            TIMER_A_CCTLN_SCS;
}

void TA0_N_IRQHandler()
{

    currentEdge = TIMER_A0->CCR[2]; //store current edge in channel 2
    period = (currentEdge - lastEdge); //difference btwn the edges is the period
    lastEdge = currentEdge; //switch edge values to start next period count


    if ((35635 < period ) && (period < 39375))
    {
        P2->OUT &= ~(BIT0 ); //Turn LED on
    }

    /*
    if (P2->IFG & BIT5) //If P2.5 had an interrupt (going from high to low)
    {
        timerA_10Hz();
    }

    else if (P2->IFG & BIT4) //If P2.4 had an interrupt (going from high to low)
    {
        timerA_14Hz();
    }

    else
    {
        //do nothing
    }

    P2->IFG &= ~(BIT4 | BIT5); //clear flags
    P2->IE |= (BIT4 | BIT5); //interrupt enable
    */
}

