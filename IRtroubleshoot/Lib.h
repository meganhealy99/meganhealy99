/*
 * Library.h
 *
 *  Created on: Jul 27, 2020
 *      Author: meganhealy
 */

#ifndef LIBRARY_H_
#define LIBRARY_H_

volatile uint16_t lastEdge;
volatile uint16_t currentEdge;
volatile uint16_t period;
volatile uint8_t detect10Hz;

void emitter_init();
void receiver_init();
void timerA_10Hz();
void timerA_14Hz();
void LED_init();
void Photo_Diode_init();
void timerA2_init();


#endif /* LIBRARY_H_ */
