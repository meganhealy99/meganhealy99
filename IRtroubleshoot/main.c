#include "msp.h"
#include "Lib.h"
/**
 * main.c
 */

void part1();
void part2();

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     /// stop watchdog timer

    int userSelect = 2;

    if (userSelect == 1){
        part1(); //lab 11 part 1
    }

    else if (userSelect == 2){
        part2(); //lab 11 part 2
    }

    else{
        //printf("Error");
    }
}

void part1(){ //Blink the IR LED @ 10Hz

    emitter_init();
    timerA_10Hz();

    while (1)
    {

    }
}

void part2(){

    //interrupt enable for different frequencies:
    NVIC_EnableIRQ(TA2_N_IRQn); //interrupt call for Timer A 2 (P2.4 & P2.5)
    //NVIC_EnableIRQ(
    __enable_irq();

    emitter_init();
    timerA_10Hz();
    LED_init();
    //receiver_init();
    timerA2_init();
    Photo_Diode_init();

    while (1)
 {

 }

}
